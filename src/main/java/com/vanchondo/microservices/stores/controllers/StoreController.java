package com.vanchondo.microservices.stores.controllers;

import com.vanchondo.microservices.stores.services.StoreService;
import com.vanchondo.vventas.entities.Store;
import com.vanchondo.vventas.jwt.CurrentUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
public class StoreController {

    private StoreService storeService;

    @DeleteMapping("")
    public void deleteStore(@RequestAttribute("currentUser") CurrentUser currentUser, @PathParam("name") String name){
        storeService.delete(currentUser, name);
    }

    @PostMapping("")
    public Store save(@RequestBody Store store){
        return storeService.save(store);
    }

    @PutMapping("")
    public Store update(@RequestAttribute("currentUser") CurrentUser currentUser, @RequestBody Store store){
        return storeService.update(currentUser, store);
    }

    @GetMapping("/")
    public Store findByName(@PathParam("store") String store){
        return storeService.findByName(store);
    }

    @PostMapping("initializeDB")
    public void initializeDB(){
        storeService.cleanDB();
    }

    @Autowired
    public void setStoreService(StoreService storeService){
        this.storeService = storeService;
    }

}
