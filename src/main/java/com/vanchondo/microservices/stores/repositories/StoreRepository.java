package com.vanchondo.microservices.stores.repositories;

import com.vanchondo.vventas.entities.Store;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StoreRepository extends MongoRepository<Store, String> {

    Store findByName(String name);
    int deleteAllByName(String name);
}
