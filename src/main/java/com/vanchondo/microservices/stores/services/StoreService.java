package com.vanchondo.microservices.stores.services;

import com.vanchondo.microservices.stores.repositories.StoreRepository;
import com.vanchondo.vventas.entities.RoleEnum;
import com.vanchondo.vventas.entities.Store;
import com.vanchondo.vventas.entities.User;
import com.vanchondo.vventas.jwt.CurrentUser;
import com.vanchondo.vventas.utilities.UserUtility;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class StoreService {

    private final static Logger logger = LoggerFactory.getLogger(StoreService.class);

    private StoreRepository storeRepository;

    public Store findByName(String name){
        logger.debug("Searching by name: "+ name);
        if (StringUtils.isEmpty(name)) {
            throw new IllegalArgumentException("Name not valid");
        }
        else{
            Store store = storeRepository.findByName(name);  //Returns null when no records are found
            logger.debug("Found: " + store);
            return store;
        }

    }

    public Store save(Store store) {
        logger.debug("Trying to save: "+ store);
        if (findByName(store.getName()) != null) {
            logger.debug("Store name already in use");
            throw new IllegalArgumentException("Store name already in use");
        }
        else
        {
            store.setUpdatedAt(new Date());
            store.setUpdatedBy("web");
            store.setDeleted(false);
            Store storeDB = storeRepository.save(store);
            logger.debug("Store saved: "+ storeDB);
            return storeDB;
        }
    }

    public Store update(CurrentUser currentUser, Store store){
        logger.debug("Trying to update: "+ store);
        if (!UserUtility.isAdmin(currentUser.getRoles())){
            logger.debug("Invalid attempt to update store: "+ store + " by the user: " + currentUser.getEmail());
            throw new IllegalArgumentException("Invalid attempt to update store");
        }
        Store storeDB = findByName(currentUser.getStore());
        if (storeDB == null) {
            logger.debug("Store "+currentUser.getStore()+" does not exist");
            throw new IllegalArgumentException("Store does not exist");
        }
        else
        {
            storeDB.setName(store.getName());
            storeDB.setUpdatedAt(new Date());
            storeDB.setUpdatedBy(currentUser.getEmail());
            storeDB.setDeleted(false);
            storeDB = storeRepository.save(storeDB);
            logger.debug("Store updated: "+ storeDB);
            return storeDB;
        }
    }

    public void delete(CurrentUser currentUser, String store){
        logger.debug("Trying to delete: " + store);
        if (!currentUser.getStore().equals(store) && !UserUtility.isAdmin(currentUser.getRoles())){
            logger.debug("Invalid attempt to delete store: "+ store + " by the user: " + currentUser.getEmail());
            throw new IllegalArgumentException("Invalid attempt to delete store");
        }
        Store storeDB = findByName(store);
        if (storeDB == null){
            logger.debug("Store not found");
            throw new IllegalArgumentException("Store not found");
        }
        else{
            storeDB.setUpdatedBy(currentUser.getEmail());
            storeDB.setUpdatedAt(new Date());
            storeDB.setDeleted(true);
            storeDB = storeRepository.save(storeDB);
            if (storeDB.isDeleted()){
                logger.debug(store + " store set as deleted ");
            }
            else{
                logger.debug(store + " store not deleted");
                throw new IllegalArgumentException("Store not deleted.");
            }
        }
    }

    public void cleanDB(){
        logger.debug("Deleting all stores");
        storeRepository.deleteAll();
    }

    @Autowired
    public void setStoreRepository(StoreRepository storeRepository){
        this.storeRepository = storeRepository;
    }
}
